#define DRV_L 2 //speed L wheel
#define DRV_R 7 //speed R wheel
#define IN_L1 3
#define IN_L2 4
#define IN_R1 5
#define IN_R2 6

#define SPD_BASE_L 0
#define SPD_BASE_R 0
#define SPD (uint8_t)((float)spd_lv/10*255)

void driveMotor();

uint8_t cmd = 0, spd_lv = 5, drt_l = 0, drt_r = 0; // drt : 0->Stop, 1->Go, 2->Back

void setup() 
{
  Serial.begin(115200); 
  pinMode(DRV_L, OUTPUT);
  pinMode(DRV_R, OUTPUT);
  pinMode(IN_L1,OUTPUT);
  pinMode(IN_L2,OUTPUT);
  pinMode(IN_R1,OUTPUT);
  pinMode(IN_R2,OUTPUT);
  driveMotor();
}

void loop() 
{
  if(Serial.available()){
    cmd = Serial.read();
    if(cmd == 'w'){
      drt_l = 1;
      drt_r = 1;
    }
    else if(cmd == 's'){
      drt_l = 2;
      drt_r = 2;
    }
    else if(cmd == 'a'){
      drt_l = 2;
      drt_r = 1;
    }
    else if(cmd == 'd'){
      drt_l = 1;
      drt_r = 2;
    }
    else if(cmd == 'b' && spd_lv < 10){
      spd_lv++;
    }
    else if(cmd == 'v' && spd_lv > 0){
      spd_lv--;
    }
    else if(cmd == 'x'){
      drt_l = 0;
      drt_r = 0;
    }

    Serial.println((char)cmd);
    Serial.print(SPD);
    Serial.print(" ");
    Serial.print((drt_l&2)>>1);
    Serial.print("|");
    Serial.print(drt_l&1);
    Serial.print(" ");
    Serial.print((drt_r&2)>>1);
    Serial.print("|");
    Serial.println(drt_r&1);
  }
  driveMotor();
}

void driveMotor()
{
  //L wheel
  analogWrite(DRV_L, SPD_BASE_L+SPD);
  digitalWrite(IN_L1, (drt_l&2)>>1);
  digitalWrite(IN_L2, drt_l&1);
  //R wheel
  analogWrite(DRV_R, SPD_BASE_R+SPD);
  digitalWrite(IN_R1, (drt_r&2)>>1);
  digitalWrite(IN_R2, drt_r&1);
}


